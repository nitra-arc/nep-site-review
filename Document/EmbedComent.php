<?php

namespace Nitra\ReviewBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections as Collections;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use Gedmo\Blameable\Traits\BlameableDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
/**
 * @MongoDB\EmbeddedDocument
 */

class EmbedComent {
    
    use BlameableDocument;
    use TimestampableDocument;

     /**
     * @MongoDB\Id(strategy="AUTO")
     * 
     */
    private $id;
    
    /**
     * @MongoDB\ReferenceOne(targetDocument="Nitra\BuyerBundle\Document\Buyer")
     */
    private $buyer;

    /**
     * 
     * @MongoDB\String
     * 
     */
    private $message;
    
     /**
     * 
     * @MongoDB\Boolean
     * 
     */
    private $status;

    /**
     * 
     * @MongoDB\Boolean
     * 
     */
    private $moderated;

    
    /**
     * 
     * @MongoDB\Field(type="date")
     * 
     */
    private $add_date;
  
     /**
     * 
     * @MongoDB\String
     * 
     */
    private $userName;
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId() {
        return $this->id;
    }

    
    
    /**
     * Set message
     *
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }


    /**
     * Set status
     *
     * @param boolean $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set moderated
     *
     * @param boolean $moderated
     * @return self
     */
    public function setModerated($moderated)
    {
        $this->moderated = $moderated;
        return $this;
    }

    /**
     * Get moderated
     *
     * @return boolean $moderated
     */
    public function getModerated()
    {
        return $this->moderated;
    }

    /**
     * Set buyer
     *
     * @param Nitra\BuyerBundle\Document\Buyer $buyer
     * @return self
     */
    public function setBuyer(\Nitra\BuyerBundle\Document\Buyer $buyer)
    {
        $this->buyer = $buyer;
        return $this;
    }

    /**
     * Get buyer
     *
     * @return Nitra\BuyerBundle\Document\Buyer $buyer
     */
    public function getBuyer()
    {
        return $this->buyer;
    }
    
    /**
     * Set addDate
     *
     * @param date $addDate
     * @return self
     */
    public function setAddDate($addDate) {
        $this->add_date = $addDate;
        return $this;
    }

    /**
     * Get addDate
     *
     * @return date $addDate
     */
    public function getAddDate() {
        return $this->add_date;
    }
    

    /**
     * Set userName
     *
     * @param string $userName
     * @return self
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * Get userName
     *
     * @return string $userName
     */
    public function getUserName()
    {
        return $this->userName;
    }
  
}

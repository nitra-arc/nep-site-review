<?php namespace Nitra\ReviewBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections as Collections;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use Gedmo\Blameable\Traits\BlameableDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @MongoDB\Document
 */
class Review
{

    use BlameableDocument;

use TimestampableDocument;

    /**
     * @MongoDB\Id(strategy="AUTO")
     * 
     */
    private $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Nitra\BuyerBundle\Document\Buyer")
     */
    private $buyer;

    /**
     * 
     * @MongoDB\Boolean
     * 
     */
    private $status;

    /**
     * 
     * @MongoDB\Boolean
     * 
     */
    private $answer_moderated;

    /**
     * 
     * @MongoDB\Boolean
     * 
     */
    private $moderated;

    /**
     * 
     * @MongoDB\Field(type="date")
     * 
     */
    private $add_date;

    /**
     * 
     * @MongoDB\String
     * @Assert\NotBlank
     */
    private $message;

    /**
     * @MongoDB\EmbedMany(targetDocument="EmbedComent" )
     */
    private $embedded_coment;

    /**
     * 
     * @MongoDB\String
     * 
     */
    private $object_id;

    /**
     * 
     * @MongoDB\String
     * 
     */
    private $reviewObjName;

    /**
     * 
     * @MongoDB\String
     * 
     */
    private $userName;

    /**
     * @MongoDB\Field(type="int")
     * @Assert\Choice(choices = {"0","1","2","3","4","5"})
     * 
     */
    private $obj_rating;

    /**
     * 
     * @MongoDB\String
     * 
     */
    private $utensils;

    public function __construct()
    {
        $this->embedded_coment = new \Doctrine\Common\Collections\ArrayCollection();
        $mongoDateObject = new \MongoDate();
        $this->setAddDate($mongoDateObject);
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set addDate
     *
     * @param date $addDate
     * @return self
     */
    public function setAddDate($addDate)
    {
        $this->add_date = $addDate;
        return $this;
    }

    /**
     * Get addDate
     *
     * @return date $addDate
     */
    public function getAddDate()
    {
        return $this->add_date;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set objectId
     *
     * @param string $objectId
     * @return self
     */
    public function setObjectId($objectId)
    {
        $this->object_id = $objectId;
        return $this;
    }

    /**
     * Get objectId
     *
     * @return string $objectId
     */
    public function getObjectId()
    {
        return $this->object_id;
    }

    /**
     * Set objRating
     *
     * @param int $objRating
     * @return self
     */
    public function setObjRating($objRating)
    {
        $this->obj_rating = $objRating;
        return $this;
    }

    /**
     * Get objRating
     *
     * @return int $objRating
     */
    public function getObjRating()
    {
        return $this->obj_rating;
    }

    /**
     * Set moderated
     *
     * @param boolean $moderated
     * @return self
     */
    public function setModerated($moderated)
    {
        $this->moderated = $moderated;
        return $this;
    }

    /**
     * Get moderated
     *
     * @return boolean $moderated
     */
    public function getModerated()
    {
        return $this->moderated;
    }

    /**
     * Add embeddedComent
     *
     * @param Nitra\ReviewBundle\Document\EmbedComent $embeddedComent
     */
    public function addEmbeddedComent(\Nitra\ReviewBundle\Document\EmbedComent $embeddedComent)
    {
        $this->embedded_coment[] = $embeddedComent;
    }

    /**
     * Remove embeddedComent
     *
     * @param Nitra\ReviewBundle\Document\EmbedComent $embeddedComent
     */
    public function removeEmbeddedComent(\Nitra\ReviewBundle\Document\EmbedComent $embeddedComent)
    {
        $this->embedded_coment->removeElement($embeddedComent);
    }

    /**
     * Get embeddedComent
     *
     * @return Doctrine\Common\Collections\Collection $embeddedComent
     */
    public function getEmbeddedComent()
    {
        return $this->embedded_coment;
    }

    /**
     * Set buyer
     *
     * @param Nitra\BuyerBundle\Document\Buyer $buyer
     * @return self
     */
    public function setBuyer(\Nitra\BuyerBundle\Document\Buyer $buyer)
    {
        $this->buyer = $buyer;
        return $this;
    }

    /**
     * Get buyer
     *
     * @return Nitra\BuyerBundle\Document\Buyer $buyer
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * Set answerModerated
     *
     * @param boolean $answerModerated
     * @return self
     */
    public function setAnswerModerated($answerModerated)
    {
        $this->answer_moderated = $answerModerated;
        return $this;
    }

    /**
     * Get answerModerated
     *
     * @return boolean $answerModerated
     */
    public function getAnswerModerated()
    {
        return $this->answer_moderated;
    }

    /**
     * Set reviewObjName
     *
     * @param string $reviewObjName
     * @return self
     */
    public function setReviewObjName($reviewObjName)
    {
        $this->reviewObjName = $reviewObjName;
        return $this;
    }

    /**
     * Get reviewObjName
     *
     * @return string $reviewObjName
     */
    public function getReviewObjName()
    {
        return $this->reviewObjName;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return self
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * Get userName
     *
     * @return string $userName
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set utensils
     *
     * @param string $utensils
     * @return self
     */
    public function setUtensils($utensils)
    {
        $this->utensils = $utensils;
        return $this;
    }

    /**
     * Get utensils
     *
     * @return string $utensils
     */
    public function getUtensils()
    {
        return $this->utensils;
    }

}

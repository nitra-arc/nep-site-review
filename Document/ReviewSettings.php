<?php

namespace Nitra\ReviewBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @MongoDB\Document 
 * 
 * 
 * 
 */
class ReviewSettings {

    /**
     * @MongoDB\Id(strategy="AUTO")
     * 
     */
    private $id;

    /**
     * 
     * @MongoDB\Boolean 
     * @Assert\NotBlank
     * 
     */
    private $show_unmoderated;

  


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set showUnmoderated
     *
     * @param boolean $showUnmoderated
     * @return self
     */
    public function setShowUnmoderated($showUnmoderated)
    {
        $this->show_unmoderated = $showUnmoderated;
        return $this;
    }

    /**
     * Get showUnmoderated
     *
     * @return boolean $showUnmoderated
     */
    public function getShowUnmoderated()
    {
        return $this->show_unmoderated;
    }
}

<?php

namespace Nitra\ReviewBundle\Controller;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nitra\StoreBundle\Lib\Globals;
use Nitra\BuyerBundle\Document\Buyer;
use Nitra\ReviewBundle\Document\EmbedComent;
use Nitra\ReviewBundle\Form\Type\CaptchaType;
use Nitra\ReviewBundle\Form\Type\UserNameAndEmailType;
use Nitra\ReviewBundle\Form\Type\ReviewType;
use Nitra\ReviewBundle\Form\Type\UserNameType;
use Nitra\ReviewBundle\Form\Type\AnswerWithEmailType;
use Nitra\ReviewBundle\Form\Type\AnswerType;

class ReviewController extends NitraController
{
    protected $validatorError = null;

    protected function getTypeCaptcha()         { return new CaptchaType();             }
    protected function getTypeNameAndEmail()    { return new UserNameAndEmailType();    }
    protected function getTypeReview()          { return new ReviewType();              }
    protected function getTypeName()            { return new UserNameType();            }
    protected function getTypeAnswer()          { return new AnswerType();              }
    protected function getTypeAnswerEmail()     { return new AnswerWithEmailType();     }

    /**
     * @Route("/reviews", name="nitra_review")
     * @Template("NitraReviewBundle:Review:review.html.twig")
     */
    public function reviewAction($utensils = null, $objectId = null, $reviewObjName = null)
    {
        return array(
            'objectId'      => $this->sessionRecord('objectId', $objectId),
            'reviewObjName' => $this->sessionRecord('reviewObjName', $reviewObjName),
            'utensils'      => $this->sessionRecord('utensils', $utensils),
            'script'        => $this->reviewScriptAction(),
        );
    }

    public function reviewInfoAction($objectId, $type, $getProductReviewCount = false, $reviewLink = '#tabs')
    {
        $qb = $this->getDocumentManager()->createQueryBuilder('NitraReviewBundle:Review')
            ->field('object_id')->equals($objectId)
            ->field('status')->equals(true);
        if (!$this->getShowUnmoderated()) {
            $qb->field('moderated')->equals(true);
        }

        $reviews = $qb->getQuery()->execute();

        $reviewsCount = $reviews->count();

        if ($getProductReviewCount) {
            return new Response($reviewsCount);
        }

        if (!$reviewsCount) {
            return new Response();
        }

        $sum = 0;
        $i   = 0;
        foreach ($reviews as $review) {
            if ($review->getObjRating() > 0) {
                $sum += $review->getObjRating();
                $i++;
            }
        }
        $j = ($i == 0) ? 1 : $i;
        $averageRating = (int) round($sum / $j);
        $template = ($type == 'page')
            ? 'NitraReviewBundle:Review:reviewBlockPage.html.twig'
            : 'NitraReviewBundle:Review:reviewBlock.html.twig';

        return $this->render($template, array(
            'reviewCount'           => $reviewsCount,
            'averageRating'         => $averageRating,
            'reviewLink'            => $reviewLink,
        ));
    }

    /**
     * @Route("/nitra_captcha", name="nitra_captcha")
     */
    public function captchaAction(Request $request)
    {
        $id = $request->get('id');
        $session = $request->getSession();
        if ($id) {
            $session->set('embedComentId', $id);
        }
        $formCaptcha = $this->createForm($this->getTypeCaptcha());
        $formCaptcha->handleRequest($request);
        if ($formCaptcha->isValid()) {
            $session->set('confirmCaptcha', true);
            $embedComentId = $session->get('embedComentId');
            $session->remove('embedComentId');
            return new JsonResponse(array(
                'isValidCaptcha'    => true,
                'id'                => $embedComentId,
            ));
        }
        return $this->render('NitraReviewBundle:Review:Captcha.html.twig', array(
            'captchaReview' => $formCaptcha->createView(),
        ));
    }

    /**
     * @Template("NitraReviewBundle:Review:reviewScript.html.twig")
     */
    public function reviewScriptAction()
    {
        return array(
            'authType'          => $this->getAuthTypeParam(),
            'showUnmoderated'   => $this->getShowUnmoderated(),
        );
    }

    /**
     * @Route("/add_review", name="add_review")
     */
    public function addReviewAction(Request $request, $utensils = null, $objectId = null, $reviewObjName = null)
    {
        $utensils        = $this->sessionRecord('utensils', $utensils);
        $objectId        = $this->sessionRecord('objectId', $objectId);
        $reviewObjName   = $this->sessionRecord('reviewObjName', $reviewObjName);
        $showUnmoderated = $this->getShowUnmoderated();
        $authType        = $this->getAuthTypeParam();
        $result          = false;

        $formOptions     = array(
            'action' => $this->generateUrl('add_review'),
            'attr'   => array(
                'id' => 'review',
            ),
        );
        $buyer = $this->getDocumentManager()->find('NitraBuyerBundle:Buyer', $request->getSession()->get('buyer', array('id' => null))['id']);
        switch ($authType) {
            case 'userNameAndEmail':
                $formAddReview = $this->createForm($this->getTypeNameAndEmail(), null, $formOptions);
                if ($buyer) {
                    $formAddReview->get('userName')->setData($buyer->getName());
                }
                $cloned = clone $formAddReview;
                $formAddReview->handleRequest($request);
                $review = $formAddReview->getData();
                break;
            case 'nitraAuth':
                $formAddReview = $this->createForm($this->getTypeReview(), null, $formOptions);
                $cloned = clone $formAddReview;
                $formAddReview->handleRequest($request);
                $review = $formAddReview->getData();
                if ($request->isMethod('POST')) {
                    $captchaCheck = $this->captchaCheck();
                    if (!$captchaCheck) {
                        return new JsonResponse(array(
                            'captchaReview' => true,
                        ));
                    }
                    if (!$buyer) {
                        return new JsonResponse(array(
                            'anonim' => true,
                        ));
                    }
                    $review->setBuyer($buyer);
                }
                break;
            case 'userName':
                $formAddReview = $this->createForm($this->getTypeName(), null, $formOptions);
                if ($buyer) {
                    $formAddReview->get('userName')->setData($buyer->getName());
                }
                $cloned = clone $formAddReview;
                $formAddReview->handleRequest($request);
                $review = $formAddReview->getData();
                break;
        }

        if ($formAddReview->isValid()) {
            if ($authType == 'userNameAndEmail') {
                $email = $formAddReview->get('email')->getData();
                $review->setBuyer($this->setBuyer($review->getUserName(), $email));
            }
            $addReviewResult = $this->addReview($utensils, $objectId, $reviewObjName, $review);
            if ($addReviewResult) {
                $formAddReview = $cloned;
                $result = true;
            }
        }

        return $this->render('NitraReviewBundle:Review:addReview.html.twig', array(
            'authType'          => $authType,
            'addReview'         => $formAddReview->createView(),
            'success'           => $result,
            'objectId'          => $objectId,
            'showUnmoderated'   => $showUnmoderated
        ));
    }

    /**
     * @Route("/answer_review", name="answer_review")
     */
    public function answerAction(Request $request)
    {
        $reviewId = $request->get('id');
        if (!$reviewId) {
            $reviewId = $request->request->get('id');
        }
        $showUnmoderated    = $this->getShowUnmoderated();
        $authType           = $this->getAuthTypeParam();
        $form               = $this->createForm(($authType == 'userNameAndEmail') ? $this->getTypeAnswerEmail() : $this->getTypeAnswer());
        $form->handleRequest($request);
        if ($form->isValid() && $reviewId) {
            $embedComent = $form->getData();
            if ($authType == 'userNameAndEmail') {
                $email = $form->get('email')->getData();
                $Buyer = $this->setBuyer($embedComent->getUserName(), $email);
                $embedComent->setBuyer($Buyer);
            }
            return $this->addAnswer($reviewId, $embedComent);
        }
        return $this->render('NitraReviewBundle:Review:answerReview.html.twig', array(
            'form'              => $form->createView(),
            'showUnmoderated'   => $showUnmoderated,
            'reviewId'          => $reviewId,
        ));
    }

    /**
     * @Route("/review_list", name="review_list")
     */
    public function listAction(Request $request)
    {
        $session            = $request->getSession();
        $authType           = $this->getAuthTypeParam();
        $answerParam        = $this->getAnswerParam();
        $showUnmoderated    = $this->getShowUnmoderated();
        $reviewId           = $request->request->get('reviewId');
        $embedComentMessage = $request->request->get('embedComent');
        $objectId           = $session->has('objectId') ? $session->get('objectId') : $request->get('objectId');
        $page               = $request->request->has('page') ? $request->request->get('page') : 1;
        $validData          = $this->Validator($reviewId, $embedComentMessage);
        if ($authType == 'nitraAuth') {
            $buyer = $session->get('buyer');
        } else {
            $buyer = false;
        }

        if ($buyer) {
            $captchaCheck = $this->captchaCheck();
            if ($validData && $captchaCheck) {
                $Buyer = $this->getDocumentManager()->find('NitraBuyerBundle:Buyer', $buyer['id']);
                $embedComent = new EmbedComent();
                $embedComent->setMessage($embedComentMessage);
                $embedComent->setBuyer($Buyer);
                return $this->addAnswer($reviewId, $embedComent);
            }
        }
        if ($request->request->has('submitEmbedComent')) {
            if ($this->validatorError) {
                return new JsonResponse($this->validatorError);
            }
            if (!$buyer) {
                return new JsonResponse(array(
                    'anonim'        => true,
                ));
            }
            if (!$captchaCheck) {
                return new JsonResponse(array(
                    'captchaReview' => true,
                ));
            }
        }
        if ($objectId) {
            $findByArr          = $this->ShowUnmoderated($showUnmoderated, $objectId);
            $limit              = Globals::getStoreLimit('review_list', 10);
            $skip               = (((int) $page > 0) ? $page * $limit : $limit) - $limit;
            $listReviewLimited  = $this->getDocumentManager()->createQueryBuilder('NitraReviewBundle:Review')
                ->setQueryArray($findByArr)
                ->skip($skip)
                ->limit($limit)
                ->sort('add_date', -1)
                ->getQuery()->execute();
            $listReviewCount    = $listReviewLimited->count();
            $nextListButton     = $listReviewCount > $limit + $skip;
        } else {
            $listReviewLimited  = null;
            $nextListButton     = false;
        }

        // Если ajax подставляем подшаблон для добавления элементов списка
        $template = $request->isXmlHttpRequest() ? 'ListReview.html.twig' : 'singleListReview.html.twig';

        return $this->render("NitraReviewBundle:Review:{$template}", array(
            'authType'          => $authType,
            'answerParam'       => $answerParam,
            'listData'          => $listReviewLimited,
            'nextListButton'    => $nextListButton,
            'showUnmoderated'   => $showUnmoderated,
        ));
    }

    protected function Validator($reviewId, $embedComentMessage)
    {
        if ($reviewId && $embedComentMessage) {
            return true;
        } elseif (!$embedComentMessage) {
            $this->validatorError = array('blankText' => true);
        }
        return false;
    }

    protected function ShowUnmoderated($showUnmoderated, $objectId)
    {
        if ($showUnmoderated) {
            $findByArr = array(
                'object_id' => $objectId,
                'status'    => true,
            );
        } else {
            $findByArr = array(
                'object_id' => $objectId,
                'status'    => true,
                'moderated' => true,
            );
        }
        return $findByArr;
    }

    protected function setBuyer($userName, $email)
    {
        $Buyer = $this->getDocumentManager()->getRepository('NitraBuyerBundle:Buyer')->findOneByEmail($email);
        if (!$Buyer) {
            $Buyer = new Buyer();
            $Buyer->setName($userName);
            $Buyer->setEmail($email);
            $this->getDocumentManager()->persist($Buyer);
            $this->getDocumentManager()->flush($Buyer);
        }
        return $Buyer;
    }

    protected function addAnswer($reviewId, $embedComent)
    {
        $authType           = $this->getAuthTypeParam();
        $answerParam        = $this->getAnswerParam();
        $session            = $this->getRequest()->getSession();
        $showUnmoderated    = $this->getShowUnmoderated();
        $embedComent->setStatus(true);
        $embedComent->setModerated(false);
        $embedComent->setAddDate(new \DateTime());
        $listReview = $this->getDocumentManager()->find('NitraReviewBundle:Review', $reviewId);
        $listReview->setAnswerModerated(false);
        $listReview->addEmbeddedComent($embedComent);
        $listCurrentReview[0] = $listReview;
        $this->getDocumentManager()->flush();
        $session->set('confirmCaptcha', false);

        return $this->render('NitraReviewBundle:Review:ListReview.html.twig', array(
            'authType'          => $authType,
            'answerParam'       => $answerParam,
            'listData'          => $listCurrentReview,
            'nextListButton'    => false,
            'showUnmoderated'   => $showUnmoderated,
        ));
    }

    protected function addReview($utensils, $objectId, $reviewObjName, $addReview)
    {
        $session = $this->getRequest()->getSession();
        $addReview->setAnswerModerated(true);
        $addReview->setObjectId($objectId);
        $addReview->setUtensils($utensils);
        $addReview->setReviewObjName($reviewObjName);
        $addReview->setStatus(true);
        $addReview->setModerated(false);
        $this->getDocumentManager()->persist($addReview);
        $this->getDocumentManager()->flush();
        $session->set('confirmCaptcha', false);

        return true;
    }

    protected function captchaCheck()
    {
        $session        = $this->getRequest()->getSession();
        $securityReview = $session->get('securityReview');
        $buyer          = $session->get('buyer', array());
        $newBuyer       = key_exists('newBuyer', $buyer) && ($buyer['newBuyer'] == 'passive');
        $captchaReview  = $securityReview['allUsersCaptchaReview'] || ($securityReview['newUsersCaptchaReview'] && $newBuyer);
        $confirmCaptcha = $session->get('confirmCaptcha');
        if (!$captchaReview) {
            $confirmCaptcha = true;
        }
        return $confirmCaptcha;
    }

    protected function getAnswerParam()
    {
        return $this->container->hasParameter('reviewAnswer') ? $this->container->getParameter('reviewAnswer') : false;
    }

    protected function getAuthTypeParam()
    {
        return $this->container->hasParameter('reviewAuth') ? $this->container->getParameter('reviewAuth') : 'userName';
    }

    /** @return \Doctrine\Common\Cache\ApcCache */
    protected function getCache() { return $this->container->get('cache_apc'); }

    protected function getShowUnmoderated()
    {
        $store = Globals::getStore();
        $cache = $this->getCache();

        $key   = $this->getMongoDatabaseName() . '_show_unmoderated_' . $store['host'];

        if ($cache->contains($key)) {
            $showUnmoderated = $this->getCache()->fetch($key);
        } else {
            $settings = $this->getDocumentManager()->getRepository('NitraReviewBundle:ReviewSettings')->findOneBy(array());
            $showUnmoderated = $settings ? $settings->getShowUnmoderated() : true;
            $this->getCache()->save($key, $showUnmoderated);
        }

        return $showUnmoderated;
    }

    protected function getStoreHost()
    {
        return $this->container->hasParameter('store_host') ? $this->container->getParameter('store_host') : '';
    }

    protected function getMongoDatabaseName()
    {
        return $this->container->hasParameter('mongo_database_name') ? $this->container->getParameter('mongo_database_name') : '';
    }

    protected function sessionRecord($recordName, $record)
    {
        $session = $this->getRequest()->getSession();
        if ($record) {
            $session->set($recordName, $record);
        } else {
            $record = $session->get($recordName);
        }

        return $record;
    }
}
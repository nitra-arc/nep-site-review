ReviewBundle
==============

Configuring 
===========================
To make this bundle work you need to add the following to your app/config/config.yml:

```yaml
# app/config/config.yml

 twig.extension.intl:
        class: Twig_Extensions_Extension_Intl
        tags:
            - { name: twig.extension }
```


Import the routing
===================

Import the routing

```yaml
# app/config/routing.yml

nitra_review:
    resource: "@NitraReviewBundle/Resources/config/routing.yml"
    prefix:   /
```

Import the routing

```yaml
# app/config/parameters.yml
parameters:
    # способ авторизации (какие поля нужны для ввода отзыва): 
    # nitraAuth - с помощью бандла NitraAuth
    # userName - для отправки сообщения требуется только имя пользователя
    # userNameAndEmail - для отправки сообщения требуется имя пользователя и email
    # отсутствие reviewAuth равноценно reviewAuth: userName, при этом варианте не используется документ BuyerBundle
    # reviewAnswer - будет ли возможность оставлять ответ на отзывы пользователям, принимает значение true, false соответственно. false по умолчанию при отсутствии параметра.

    reviewAuth: userNameAndEmail
    reviewAnswer: true
```

Enable the bundle
==================

Enable the bundle in the kernel:

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Nitra\ReviewBundle\NitraReviewBundle(),
    );
}
```

### Пример добавления в шаблон (карточка товара - табы)

```twig
    # src/Nitra/ProductBundle/Resources/views/Product/productPageContent.html.twig

    # ...
    {% block tabs %}
        # ...
        {% set productReviews = render(controller("NitraReviewBundle:Review:review", {'utensils' : 'product', 'objectId': product.id, 'reviewObjName':product.name}))%}
        # ...
        {{ include('NitraProductBundle:Modules:tabs.html.twig', {'tabs': {
            # ...
            'reviews': { 'content': productReviews, 'name': ('product.review'|trans) }
            # ...
        }}) }}
    {% endblock tabs %}
    # ...
```

Есть возможность выводить отдельно форму, отдельно список отзывов

```twig
    # src/Nitra/ProductBundle/Resources/views/Product/productPageContent.html.twig

    # ...
    {% Вывод списка %}
    {% block tabs %}
        # ...
        {% set productReviews = render(controller("NitraReviewBundle:Review:List"))%}
        # ...
        {{ include('NitraProductBundle:Modules:tabs.html.twig', {'tabs': {
            # ...
            'reviews': { 'content': productReviews, 'name': ('product.review'|trans) }
            # ...
        }}) }}
    {% endblock tabs %}

    {% Вывод формы %}
    {% render controller("NitraReviewBundle:Review:addReview", {'utensils' : 'product', 'objectId': product.id, 'reviewObjName':product.name}) %}

    {% Необходимый скрипт для работы отзывов %}
    {% render controller("NitraReviewBundle:Review:reviewScript") %}
    # Параметр "utensils" обозначает принадлежность отзыва к товару, либо информационной статье, либо это отзыв магазина и может принимать 3 значения: product, info, store.
    # В зависимости от установленного параметра utensils формируется ссылка на объект отзыва на форме редактирования отзыва.
    # utensils задаётся только в review и addReview action.
    #
    # Также необходимо учитывать фоктор кэширования страниц, где происходит render, в таком случае необходимо вместо {% render(controller( ... использовать
    # {{ render_esi (controller(...
    # Например так: {{ render_esi (controller("NitraReviewBundle:Review:review" , {'utensils' : 'product', 'objectId': product.id, 'reviewObjName': product.fullName })) }}
    # render_esi нельзя присваивать переменным и нежелательно(необходимо тестировать) использовать фильтра.
    #...
```
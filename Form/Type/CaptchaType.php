<?php

namespace Nitra\ReviewBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CaptchaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('captcha', 'genemu_captcha', array(
            'label'         => 'captcha.label',
        ));
        $builder->add('submit', 'submit', array(
            'label'         => 'captcha.submit',
        ));
    }

    public function getName()
    {
        return 'captchaReview';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraReviewBundle',
        ));
    }
}
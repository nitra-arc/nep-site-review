<?php

namespace Nitra\ReviewBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;

class AnswerWithEmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('userName', 'text', array(
            'label'         => 'review.comment.name',
            'constraints'   => array(
                new Constraints\NotBlank(),
            ),
        ));
        $builder->add('email', 'email', array(
            'label'         => 'review.comment.email',
            'mapped'        => false,
            'constraints'   => array(
                new Constraints\Email(),
                new Constraints\NotBlank(),
            ),
        ));
        $builder->add('message', 'textarea', array(
            'label'         => 'review.comment.label',
            'help'          => 'review.comment.help',
        ));
        $builder->add('send', 'submit', array(
            'label'         => 'review.answer.button',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'Nitra\ReviewBundle\Document\EmbedComent',
            'translation_domain'    => 'NitraReviewBundle',
        ));
    }

    public function getName()
    {
        return 'answer_email_review';
    }
}
<?php

namespace Nitra\ReviewBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;

class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message', 'textarea', array(
            'label'         => 'review.comment.label',
            'help'          => 'review.comment.help',
        ));
        $builder->add('obj_rating', 'hidden', array(
            'attr'          => array(
                'value'         => 0
            ),
            'constraints'   => array(
                new Constraints\Choice(array(0, 1, 2, 3, 4, 5))
            ),
        ));
        $builder->add('send', 'submit', array(
            'label'         => 'review.comment.button',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'Nitra\ReviewBundle\Document\Review',
            'translation_domain'    => 'NitraReviewBundle',
        ));
    }

    public function getName()
    {
        return 'review';
    }
}